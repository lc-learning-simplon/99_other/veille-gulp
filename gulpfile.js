var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync");

gulp.task("hello", function() {
    console.log("Salut Maurice !");
});

gulp.task("sass", function() {
    return gulp.src("app/scss/**/*.scss") // Récupère les fichiers sources avec gulp.src
        .pipe(sass()) // Envoie ces fichiers à travers un plugin gulp
        .pipe(gulp.dest("app/css")) // Sort les fichiers dans le dossier de destination
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task("watch", ["browserSync", "sass"], function() {
    gulp.watch("app/scss/**/*.scss", ["sass"]);
    gulp.watch("app/*.html", browserSync.reload);
    gulp.watch("app/js/**/*.js", browserSync.reload);

});

gulp.task("browserSync", function() {
    browserSync({
        server: {
            baseDir: "app"
        },
    })
});